package compassites.shopkart.view.interfaces;

import compassites.shopkart.model.pojo.ProductListData;

/**
 * Created by Shaik Atif on 14-04-2017.
 */

public interface IProductList {

    void onProductSelected(int  position);
}
