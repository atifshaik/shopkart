package compassites.shopkart.view.ui.product;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import javax.inject.Inject;

import compassites.shopkart.R;
import compassites.shopkart.core.ShopKartApplication;
import compassites.shopkart.model.pojo.ProductListData;
import compassites.shopkart.presenter.interfaces.ProductDetailView;
import compassites.shopkart.presenter.viewpresenters.ProductDetailPresenter;
import compassites.shopkart.view.ui.base.ShopKartBaseActivity;

/**
 * Created by Shaik Atif on 13-04-2017.
 */

public class ProductDescriptionActivity extends ShopKartBaseActivity implements ProductDetailView {


    private int itemPostition;
    private Dialog progressDialog;
    @Inject
    public ProductDetailPresenter productDetailPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_description);
        getActivityComponent().inject(this);
        getIntentData();
        initPresenter();
        setUpToolbar();
        initUI();

    }

    private void initPresenter() {
        productDetailPresenter.attachView(this);
    }

    private void getIntentData() {
        itemPostition= getIntent().getIntExtra(getString(R.string.product_list_data_key),0);
    }

    protected void setUpToolbar(){
        super.setUpToolbar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
    @Override
    protected void initUI() {
       productDetailPresenter.getProduct(itemPostition);
    }

    private void setUI(ProductListData productListData) {
        ImageView imageProduct = (ImageView) findViewById(R.id.image_product);
        TextView textProductName = (TextView) findViewById(R.id.text_product_name);
        TextView textProductPrice = (TextView) findViewById(R.id.text_product_price);
            Glide.with(this).load(productListData.getProductUrls().get(0).getProductURL()).into(imageProduct);
        textProductName.setText(productListData.getProductTitle());
        textProductPrice.setText(String.format("%s%s", getString(R.string.ruppee), productListData.getProductPrice()));
    }

    @Override
    public void showProgressDialog() {
        if(progressDialog==null&&!this.isDestroyed()){
            progressDialog= ProgressDialog.show(this,getString(R.string.pd_loading),getString(R.string.pd_loading_desc));
        }else if(progressDialog!=null&&!this.isDestroyed()){
            progressDialog.show();
        }

    }

    @Override
    public void hideProgressDialog() {
        if(progressDialog!=null&&progressDialog.isShowing()&&!this.isDestroyed()){
            progressDialog.dismiss();
        }
    }


    @Override
    public void showNetworkError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadActivity(ProductListData productListData) {
            setUI(productListData);
    }
}
