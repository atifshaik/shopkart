package compassites.shopkart.view.ui.product;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import javax.inject.Inject;

import compassites.shopkart.R;
import compassites.shopkart.model.pojo.ProductListData;
import compassites.shopkart.model.pojo.ProductResponseObject;
import compassites.shopkart.presenter.contants.APINames;
import compassites.shopkart.presenter.interfaces.ProductListPresenterView;
import compassites.shopkart.presenter.viewpresenters.ProductPresenter;
import compassites.shopkart.view.interfaces.IProductList;
import compassites.shopkart.view.ui.base.ShopKartBaseActivity;

public class ProductListActivity extends ShopKartBaseActivity implements IProductList,ProductListPresenterView {

    @Inject
    public ProductListAdapter productAdapter;
    @Inject
    public ProductPresenter productPresenter;

    private ProgressDialog progressDialog;
    private RecyclerView recyclerProductList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        getActivityComponent().inject(this);
        setUpToolbar();
        attachView();
        initUI();
        getProductListData();

    }

    private void attachView() {
        productPresenter.attachView(this);
    }

    protected void setUpToolbar() {
        super.setUpToolbar();
        if (actionBar != null) {
            actionBar.setTitle(getString(R.string.product_list_page_ab_title));
        }
    }

    @Override
    protected void initUI() {
        recyclerProductList = (RecyclerView) findViewById(R.id.recycler_product_list);
    }

    private void getProductListData() {
        productPresenter.getProducts(APINames.GET_PRODUCTS, false);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.refresh, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.refresh) {
            productPresenter.getProducts(APINames.GET_PRODUCTS, true);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void showProgressDialog() {
        if(progressDialog==null&&!this.isDestroyed()){
            progressDialog= ProgressDialog.show(this,getString(R.string.pd_loading),getString(R.string.pd_loading_desc));
        }else if(progressDialog!=null&&!this.isDestroyed()){
            progressDialog.show();
        }

    }

    @Override
    public void hideProgressDialog() {
        if(progressDialog!=null&&progressDialog.isShowing()&&!this.isDestroyed()){
            progressDialog.dismiss();
        }
    }


    @Override
    public void showNetworkError(String message, String requestAPIName) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadAdapter(ProductResponseObject productResponseObject) {
        if (productResponseObject.getProducts() != null && productResponseObject.getProducts().size() > 0) {
                productAdapter.setProducts(productResponseObject.getProducts());
                productAdapter.setiProductList(this);
                recyclerProductList.setLayoutManager(new LinearLayoutManager(this));
                recyclerProductList.setAdapter(productAdapter);
        }
    }

    @Override
    public void onProductSelected(int position) {
        Intent intent = new Intent(this, ProductDescriptionActivity.class);
        intent.putExtra(getString(R.string.product_list_data_key), position);
        startActivity(intent);
    }
}
