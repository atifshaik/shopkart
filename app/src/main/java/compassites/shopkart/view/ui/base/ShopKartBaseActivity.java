package compassites.shopkart.view.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import compassites.shopkart.core.ShopKartApplication;
import compassites.shopkart.di.components.ActivityComponent;
import compassites.shopkart.di.components.DaggerActivityComponent;
import compassites.shopkart.di.components.ShopAppComponent;
import compassites.shopkart.di.modules.ActivityModule;

/**
 * Created by Shaik Atif on 13-04-2017.
 */

public abstract class ShopKartBaseActivity extends AppCompatActivity {
    protected ActionBar actionBar;
    private ActivityComponent activityComponent;


    protected  void setUpToolbar(){
        actionBar=getSupportActionBar();
    }

    protected abstract void initUI();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent= DaggerActivityComponent.builder().shopAppComponent(ShopKartApplication.get(this).getApplicationComponent()).activityModule(new ActivityModule(this)).build();
    }
    public ActivityComponent getActivityComponent(){
        return activityComponent;
    }



}

