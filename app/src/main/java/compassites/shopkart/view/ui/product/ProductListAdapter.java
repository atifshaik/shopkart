package compassites.shopkart.view.ui.product;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import compassites.shopkart.R;
import compassites.shopkart.di.qualifiers.ActivityContext;
import compassites.shopkart.model.pojo.ProductListData;
import compassites.shopkart.view.interfaces.IProductList;
import io.realm.RealmList;

/**
 * Created by Shaik Atif on 13-04-2017.
 */

class ProductListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ProductListData> productListDatas;
    private IProductList iProductList;
    private Context context;

    @Inject
    public ProductListAdapter(@ActivityContext Context context){
        this.context=context;
    }


    public void setProductListDatas(List<ProductListData> productListDatas){
        this.productListDatas=new ArrayList<>(productListDatas);

    }

    public void setiProductList(IProductList iProductList){
        this.iProductList=iProductList;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product,parent,false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ProductViewHolder productViewHolder= (ProductViewHolder) holder;
        final ProductListData productListData=productListDatas.get(position);
        if(productListData.getProductUrls()==null||productListData.getProductUrls().size()==0) {
            Glide.with(context).load(productListData.getProductUrl()[0]).into(productViewHolder.imageProduct);
        }else {
            Glide.with(context).load(productListData.getProductUrls().get(0).getProductURL()).into(productViewHolder.imageProduct);
        }
        productViewHolder.textProductName.setText(productListData.getProductTitle());
        productViewHolder.textProductCategory.setText(productListData.getProductMainCategory()+" -> "+productListData.getProductSubCategory()+" -> "+ productListData.getProductCategory());
        productViewHolder.textProductPrice.setText(String.format("%s%s", context.getString(R.string.ruppee), productListData.getProductPrice()));
        productViewHolder.textProductDiscount.setText(String.format("%s%% off", productListData.getProductDiscount()));
        productViewHolder.imageProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iProductList.onProductSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productListDatas.size();
    }

    void setProducts(RealmList<ProductListData> products) {
        this.productListDatas=new ArrayList<>(products);
        notifyDataSetChanged();
    }



    private class ProductViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageProduct;
        private TextView textProductName;
        private TextView textProductCategory;
        private LinearLayout linearProductPrice;
        private TextView textProductPrice;
        private TextView textProductDiscount;

        ProductViewHolder(View view) {
            super(view);
            imageProduct = (ImageView) view.findViewById(R.id.image_product);
            textProductName = (TextView) view.findViewById(R.id.text_product_name);
            textProductCategory = (TextView) view.findViewById(R.id.text_product_category);
            linearProductPrice = (LinearLayout) view.findViewById(R.id.linear_product_price);
            textProductPrice = (TextView) view.findViewById(R.id.text_product_price);
            textProductDiscount = (TextView) view.findViewById(R.id.text_product_discount);
        }
    }

}
