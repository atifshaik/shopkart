package compassites.shopkart.core;

import android.app.Application;
import android.content.Context;

import compassites.shopkart.di.components.DaggerShopAppComponent;
import compassites.shopkart.di.components.ShopAppComponent;
import compassites.shopkart.di.modules.NetworkModule;
import compassites.shopkart.di.modules.ShopAppModule;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Shaik Atif on 13-04-2017.
 */

public class ShopKartApplication extends Application {

    private static ShopKartApplication instance;
    private ShopAppComponent shopAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        shopAppComponent= DaggerShopAppComponent.builder().shopAppModule(new ShopAppModule(this)).networkModule(new NetworkModule()).build();
        initRealm();

    }

    private void initRealm() {
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("shop.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    public static ShopKartApplication getShopKartApplication() {
        return instance;
    }

    public static ShopKartApplication get(Context context) {
        return (ShopKartApplication) context.getApplicationContext();
    }

    public ShopAppComponent getApplicationComponent() {
        return shopAppComponent;
    }
}
