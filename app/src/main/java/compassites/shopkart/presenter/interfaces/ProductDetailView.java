package compassites.shopkart.presenter.interfaces;

import compassites.shopkart.model.pojo.ProductListData;
import compassites.shopkart.model.pojo.ProductResponseObject;

/**
 * Created by Shaik Atif on 20-04-2017.
 */

public interface ProductDetailView {

    void showProgressDialog();

    void hideProgressDialog();

    void showNetworkError(String message);

    void loadActivity(ProductListData productListData);
}
