package compassites.shopkart.presenter.interfaces;

import compassites.shopkart.model.pojo.ProductResponseObject;

public interface ProductListPresenterView {

    void showProgressDialog();

    void hideProgressDialog();

    void showNetworkError(String message,String requestAPIName);

    void loadAdapter(ProductResponseObject productResponseObject);

}