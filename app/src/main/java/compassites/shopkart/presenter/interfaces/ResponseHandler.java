package compassites.shopkart.presenter.interfaces;

/**
 * Created by Shaik Atif on 13-04-2017.
 */

public interface ResponseHandler {

    void onSuccess(Object responseObject,String requestAPIName);
    void onFailure(Throwable throwable,String requestAPIName);


}
