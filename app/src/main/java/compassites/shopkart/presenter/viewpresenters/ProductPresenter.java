package compassites.shopkart.presenter.viewpresenters;

import javax.inject.Inject;

import compassites.shopkart.di.scope.PerActivity;
import compassites.shopkart.model.datamanager.DataManager;
import compassites.shopkart.model.observable.APIObservable;
import compassites.shopkart.model.pojo.ProductResponseObject;
import compassites.shopkart.presenter.interfaces.ResponseHandler;
import compassites.shopkart.presenter.interfaces.ProductListPresenterView;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Shaik Atif on 13-04-2017.
 */
@PerActivity
public class ProductPresenter extends BasePresenter implements ResponseHandler {

    private ProductListPresenterView productListPresenterView;
    private DataManager dataManager;

    @Inject
    public ProductPresenter(DataManager dataManager){
        this.dataManager=dataManager;
    }

    public void attachView(ProductListPresenterView productListPresenterView){
        this.productListPresenterView=productListPresenterView;
        dataManager.setResponseHandler(this);
    }

    public void getProducts(String requestAPIName,boolean forceNetwork){
            productListPresenterView.showProgressDialog();
            Observable<ProductResponseObject> productResponseObjectObservable=dataManager.getProducts(requestAPIName,forceNetwork);
            productResponseObjectObservable.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new APIObservable<ProductResponseObject>(this, requestAPIName));
    }


    @Override
    public void onSuccess(Object responseObject, String requestAPIName) {
        productListPresenterView.hideProgressDialog();
        productListPresenterView.loadAdapter((ProductResponseObject) responseObject);
    }

    @Override
    public void onFailure(Throwable throwable, String requestAPIName) {
        productListPresenterView.hideProgressDialog();
        productListPresenterView.showNetworkError(getErrorMessage(throwable),requestAPIName);
    }


}
