package compassites.shopkart.presenter.viewpresenters;

import javax.inject.Inject;

import compassites.shopkart.di.scope.PerActivity;
import compassites.shopkart.model.datamanager.DataManager;
import compassites.shopkart.model.observable.APIObservable;
import compassites.shopkart.model.pojo.ProductListData;
import compassites.shopkart.model.pojo.ProductResponseObject;
import compassites.shopkart.presenter.interfaces.ProductDetailView;
import compassites.shopkart.presenter.interfaces.ResponseHandler;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Shaik Atif on 20-04-2017.
 */
@PerActivity
public class ProductDetailPresenter extends BasePresenter implements ResponseHandler {

    private DataManager dataManager;
    private ProductDetailView productDetailView;

    @Inject
    public ProductDetailPresenter(DataManager dataManager){
        this.dataManager=dataManager;
    }

    public void attachView(ProductDetailView productDetailView){
        this.productDetailView=productDetailView;
        dataManager.setResponseHandler(this);
    }

    public void getProduct(int position){
        productDetailView.showProgressDialog();
        Observable<ProductListData> productResponseObjectObservable=dataManager.getProductItem(position);
        productResponseObjectObservable.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new APIObservable<ProductListData>(this,"Product Detail"));
    }

    @Override
    public void onSuccess(Object responseObject, String requestAPIName) {
        productDetailView.hideProgressDialog();
        productDetailView.loadActivity((ProductListData) responseObject);
    }

    @Override
    public void onFailure(Throwable throwable, String requestAPIName) {
        productDetailView.hideProgressDialog();
        productDetailView.showNetworkError(getErrorMessage(throwable));
    }
}
