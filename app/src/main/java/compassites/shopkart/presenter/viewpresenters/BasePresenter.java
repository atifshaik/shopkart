package compassites.shopkart.presenter.viewpresenters;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.concurrent.TimeoutException;

import compassites.shopkart.core.ShopKartApplication;
import compassites.shopkart.presenter.contants.ErrorConstants;

/**
 * Created by Shaik Atif on 13-04-2017.
 */

class BasePresenter {

    String getErrorMessage(Throwable throwable) {
        if(throwable instanceof NullPointerException){
            return ErrorConstants.INTERNETERROR;
        }
        if (throwable instanceof TimeoutException) {
            return ErrorConstants.TIMEOUT;
        }
        return ErrorConstants.GENERICERROR;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ShopKartApplication.getShopKartApplication().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
