package compassites.shopkart.presenter.contants;

/**
 * Created by Shaik Atif on 13-04-2017.
 */

public class ErrorConstants {
    public static final String TIMEOUT = "Unable to connect to server, Please verify your internet connection and try again.";
    public static final String GENERICERROR = "Oops something went wrong.We are working on this.";
    public static final String INTERNETERROR = "Please check your internet connection and try again.";
}
