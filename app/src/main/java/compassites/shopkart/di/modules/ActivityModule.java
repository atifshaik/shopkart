package compassites.shopkart.di.modules;

import android.app.Activity;
import android.content.Context;

import compassites.shopkart.di.qualifiers.ActivityContext;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Shaik Atif on 21-04-2017.
 */
@Module
public class ActivityModule {
    private Activity activity;

    public ActivityModule(Activity activity){
        this.activity=activity;
    }

    @Provides Activity getActivity(){
        return activity;
    }

    @Provides
    @ActivityContext
    Context getActivityContext(){
        return activity;
    }
}
