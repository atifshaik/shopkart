package compassites.shopkart.di.modules;

import javax.inject.Singleton;

import compassites.shopkart.model.api.ProductAPI;
import compassites.shopkart.model.retrofit.RetrofitAdapter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Shaik Atif on 20-04-2017.
 */
@Module
public class NetworkModule {

    @Provides
    @Singleton
    ProductAPI getProductAPI(){
        return RetrofitAdapter.getRetrofitAdapter().create(ProductAPI.class);
    }

}
