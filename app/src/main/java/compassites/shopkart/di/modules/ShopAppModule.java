package compassites.shopkart.di.modules;

import android.content.Context;

import javax.inject.Singleton;

import compassites.shopkart.core.ShopKartApplication;
import compassites.shopkart.di.qualifiers.ApplicationContext;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Shaik Atif on 20-04-2017.
 */

@Module
public class ShopAppModule {
    private ShopKartApplication shopKartApplication;

    public ShopAppModule(ShopKartApplication shopKartApplication){
        this.shopKartApplication=shopKartApplication;
    }
    @Provides @Singleton  ShopKartApplication getShopKartApplication() {
        return shopKartApplication;
    }
    @Provides @Singleton @ApplicationContext
    Context getShopKartApplicationContext() {
        return shopKartApplication.getApplicationContext();
    }

}

