package compassites.shopkart.di.components;

import android.app.Activity;
import android.content.Context;

import javax.inject.Singleton;

import compassites.shopkart.di.modules.ActivityModule;
import compassites.shopkart.di.qualifiers.ActivityContext;
import compassites.shopkart.di.qualifiers.ApplicationContext;
import compassites.shopkart.di.scope.PerActivity;
import compassites.shopkart.view.ui.product.ProductDescriptionActivity;
import compassites.shopkart.view.ui.product.ProductListActivity;
import dagger.Component;

/**
 * Created by Shaik Atif on 21-04-2017.
 */
@PerActivity
@Component (dependencies = ShopAppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(ProductListActivity productListctivity);

    void inject(ProductDescriptionActivity  productDescriptionActivity);

    @ActivityContext Context context();

    Activity activity();

}
