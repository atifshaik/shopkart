package compassites.shopkart.di.components;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import compassites.shopkart.core.ShopKartApplication;
import compassites.shopkart.di.modules.NetworkModule;
import compassites.shopkart.di.modules.ShopAppModule;
import compassites.shopkart.di.qualifiers.ApplicationContext;
import compassites.shopkart.model.api.ProductAPI;
import compassites.shopkart.model.datamanager.DataManager;
import compassites.shopkart.model.db.DBHelper;
import dagger.Component;

/**
 * Created by Shaik Atif on 20-04-2017.
 */
@Singleton
@Component(modules={ShopAppModule.class, NetworkModule.class})
public interface ShopAppComponent {

    @ApplicationContext Context applicationContext();

    ShopKartApplication shopKartApplication();

    ProductAPI productAPI();

    DataManager datamanager();

    DBHelper dbHelper();
}
