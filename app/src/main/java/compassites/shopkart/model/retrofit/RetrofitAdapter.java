package compassites.shopkart.model.retrofit;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Shaik Atif on 13-04-2017.
 */

public class RetrofitAdapter {
    private static final String BASE_URL = "https://api.myjson.com/bins/";

    public static Retrofit getRetrofitAdapter(){
       /* HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);*/
        OkHttpClient.Builder builder=new OkHttpClient().newBuilder();
        builder.readTimeout(15, TimeUnit.SECONDS);
        builder.connectTimeout(15, TimeUnit.SECONDS);
        OkHttpClient okHttpClient=builder.build();
        return new Retrofit.Builder()
                          .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                          .addConverterFactory(GsonConverterFactory.create())
                          .client(okHttpClient)
                          .baseUrl(BASE_URL)
                          .build();


    }
}
