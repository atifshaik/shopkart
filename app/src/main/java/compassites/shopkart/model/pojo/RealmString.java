package compassites.shopkart.model.pojo;

import io.realm.RealmObject;

/**
 * Created by Shaik Atif on 13-04-2017.
 */

public class RealmString extends RealmObject {

    public RealmString(){

    }
    private String name;

    public String getProductURL() {
        return name;
    }

    public void setProductURL(String productURL) {
        this.name = productURL;
    }
}

