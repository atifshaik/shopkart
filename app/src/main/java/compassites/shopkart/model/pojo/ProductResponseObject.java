package compassites.shopkart.model.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ProductResponseObject extends RealmObject{

@PrimaryKey
private String category;
private RealmList<ProductListData>  products;

public String getCategory() {
return category;
}

public void setCategory(String category) {
this.category = category;
}

public RealmList<ProductListData> getProducts() {
return products;
}

public void setProducts(RealmList<ProductListData>  products) {
this.products = products;
}



}