package compassites.shopkart.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;

/**
 * Created by Shaik Atif on 13-04-2017.
 */

public class ProductListData extends RealmObject implements Parcelable {

    public ProductListData(){
    }

    private Integer productDefault;
    private Integer imageDefault;
    private Integer childViewType;
    private String productCode;
    private String productPrice;
    private String productActualPrice;
    private String productDiscount;
    private String productTitle;
    private String productMainCategory;
    private String productSubCategory;
    private String productCategory;
    private RealmList<RealmString> productUrls;
    @Ignore
    private String[] productUrl;

    public String[] getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String[] productUrl) {
        this.productUrl = productUrl;
    }

    public Integer getProductDefault() {
        return productDefault;
    }

    public void setProductDefault(Integer productDefault) {
        this.productDefault = productDefault;
    }

    public Integer getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(Integer imageDefault) {
        this.imageDefault = imageDefault;
    }

    public Integer getChildViewType() {
        return childViewType;
    }

    public void setChildViewType(Integer childViewType) {
        this.childViewType = childViewType;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductActualPrice() {
        return productActualPrice;
    }

    public void setProductActualPrice(String productActualPrice) {
        this.productActualPrice = productActualPrice;
    }

    public String getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(String productDiscount) {
        this.productDiscount = productDiscount;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getProductMainCategory() {
        return productMainCategory;
    }

    public void setProductMainCategory(String productMainCategory) {
        this.productMainCategory = productMainCategory;
    }

    public String getProductSubCategory() {
        return productSubCategory;
    }

    public void setProductSubCategory(String productSubCategory) {
        this.productSubCategory = productSubCategory;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public RealmList<RealmString> getProductUrls() {
        return productUrls;
    }

    public void setProductUrls(RealmList<RealmString> productUrl) {
        this.productUrls = productUrl;
    }

    public void setRealmURLs(){
        RealmList<RealmString> productUrls=new RealmList<>();
        for(int index=0;index<=productUrl.length-1;index++){
            RealmString realmString=new RealmString();
            realmString.setProductURL(productUrl[index]);
            productUrls.add(realmString);
        }
        setProductUrls(productUrls);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
