package compassites.shopkart.model.db;

import javax.inject.Inject;
import javax.inject.Singleton;

import compassites.shopkart.di.scope.PerActivity;
import compassites.shopkart.model.pojo.ProductListData;
import compassites.shopkart.model.pojo.ProductResponseObject;
import io.realm.Realm;
import io.realm.RealmResults;
import rx.Observable;
import rx.functions.Func0;

/**
 * Created by Shaik Atif on 13-04-2017.
 */
@Singleton
public class DBHelper {

    @Inject DBHelper(){

    }
    public Observable<ProductResponseObject> getProductsFromDB(){
        Realm realm=Realm.getDefaultInstance();
        RealmResults<ProductResponseObject> productResponseObjectsRealmResult;
        productResponseObjectsRealmResult=realm.where(ProductResponseObject.class).equalTo("category","Men Shirts").findAll();
        if(productResponseObjectsRealmResult!=null&&productResponseObjectsRealmResult.size()>0) {
            final ProductResponseObject productResponseObject = productResponseObjectsRealmResult.get(0);
            return Observable.just(productResponseObject);
        }else{
            return null;
        }


    }

    public Observable<ProductListData> getProductItem(int postion){
        Realm realm=Realm.getDefaultInstance();
        RealmResults<ProductResponseObject> productResponseObjectsRealmResult;
        productResponseObjectsRealmResult=realm.where(ProductResponseObject.class).equalTo("category","Men Shirts").findAll();
        if(productResponseObjectsRealmResult!=null&&productResponseObjectsRealmResult.size()>0) {
            final ProductResponseObject productResponseObject = productResponseObjectsRealmResult.get(0);
            if(productResponseObject.getProducts()!=null&&productResponseObject.getProducts().size()>0){
                return Observable.just(productResponseObject.getProducts().get(postion));
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    public void storeProducts(ProductResponseObject productResponseObject) {
        Realm realm = null;
        try {

            realm = Realm.getDefaultInstance();
            RealmResults<ProductResponseObject> productResponseObjectsRealmResult;
            productResponseObjectsRealmResult=realm.where(ProductResponseObject.class).equalTo("category","Men Shirts").findAll();
            realm.beginTransaction();
            if(productResponseObjectsRealmResult!=null&&productResponseObjectsRealmResult.size()>0){
                productResponseObjectsRealmResult.deleteAllFromRealm();
            }
            realm.copyToRealm(productResponseObject);
            realm.commitTransaction();
        } finally {
            if(realm != null) {
                realm.close();
            }
        }
    }


}
