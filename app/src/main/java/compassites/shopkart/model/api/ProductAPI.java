package compassites.shopkart.model.api;

import compassites.shopkart.model.pojo.ProductResponseObject;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Shaik Atif on 13-04-2017.
 */

public interface ProductAPI {

    @GET("njwbv")
    Observable<ProductResponseObject> getProducts();
}
