package compassites.shopkart.model.datamanager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import compassites.shopkart.di.scope.PerActivity;
import compassites.shopkart.model.api.ProductAPI;
import compassites.shopkart.model.db.DBHelper;
import compassites.shopkart.model.observable.APIObservable;
import compassites.shopkart.model.pojo.ProductListData;
import compassites.shopkart.model.pojo.ProductResponseObject;
import compassites.shopkart.model.retrofit.RetrofitAdapter;
import compassites.shopkart.presenter.contants.APINames;
import compassites.shopkart.presenter.interfaces.ResponseHandler;
import io.realm.RealmList;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Shaik Atif on 13-04-2017.
 */
@Singleton
public class DataManager implements ResponseHandler {
    private DBHelper dbHelper;
    private ResponseHandler responseHandler;

    @Inject
    public DataManager(DBHelper dbHelper){
        this.dbHelper=dbHelper;
    }

    public void setResponseHandler(ResponseHandler responseHandler){
        this.responseHandler=responseHandler;
    }

    public Observable<ProductResponseObject> getProducts(String requestAPIName,boolean forceNetwork){
        Observable<ProductResponseObject> responseObject;
        if(dbHelper.getProductsFromDB()==null||forceNetwork){
            responseObject=RetrofitAdapter.getRetrofitAdapter().create(ProductAPI.class).getProducts();
            responseObject.subscribeOn(Schedulers.newThread()).observeOn(Schedulers.newThread()).subscribe(new APIObservable<ProductResponseObject>(this, requestAPIName));
        }else {
            responseObject=dbHelper.getProductsFromDB();
        }
        return responseObject;
    }


    public Observable<ProductListData> getProductItem(int position){
        Observable<ProductListData> productListDataObservable=dbHelper.getProductItem(position);
        if(productListDataObservable!=null){
            return productListDataObservable;
        }else{
            return null;
        }
    }

    @Override
    public void onSuccess(Object responseObject, String requestAPIName) {
        switch (requestAPIName){
            case APINames.GET_PRODUCTS:
                ProductResponseObject productResponseObject= (ProductResponseObject) responseObject;
                dbHelper.storeProducts(getProductsAfterSettingRealmURLs(productResponseObject));
        }
    }

    private ProductResponseObject getProductsAfterSettingRealmURLs(ProductResponseObject productResponseObject) {
        RealmList<ProductListData> productListDataWithRealmURLs= new RealmList<>();
        for(ProductListData productListDataItem:productResponseObject.getProducts()){
            productListDataItem.setRealmURLs();
            productListDataWithRealmURLs.add(productListDataItem);
        }
        productResponseObject.setProducts(productListDataWithRealmURLs);
        return productResponseObject;
    }

    @Override
    public void onFailure(Throwable throwable, String requestAPIName) {
        responseHandler.onFailure(throwable,requestAPIName);
    }

}
