package compassites.shopkart.model.observable;

import compassites.shopkart.presenter.interfaces.ResponseHandler;
import rx.Observer;
import rx.Subscriber;

public class APIObservable<T> extends Subscriber<T> implements Observer<T> {

    private ResponseHandler IResponseHandler;
    private String responseType;


    public APIObservable(ResponseHandler IResponseHandler,String responseType){
        this.IResponseHandler=IResponseHandler;
        this.responseType=responseType;
    }


    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        IResponseHandler.onFailure(e,responseType);
    }

    @Override
    public void onNext(Object o) {
        IResponseHandler.onSuccess(o,responseType);
    }
}
